# Expert Tech Modpack

A Minecraft modpack all about tech and automation.

## Mods
    > FoamFix
       > by asiekierka @ https://minecraft.curseforge.com/projects/foamfix-for-minecraft
    > BetterFps
        > By Guichaguri @ https://minecraft.curseforge.com/projects/betterfps
    > IC²
        > by the IC² Dev Team @ http://www.industrial-craft.net/
    > CraftTweaker
        > by jaredlll08, kindlich, and StanH @ https://minecraft.curseforge.com/projects/crafttweaker?page=18#c3
    > ProjectE
        > by sinkillerj @ https://minecraft.curseforge.com/projects/projecte
    > Mekanism, MekanismGenerators, and MekanismTools
        > by aidancbrandy
    > Buildcraft and Buildcraft Compat
        > by AlexIIL, afdw, SpaceToad, SirSengir, CovertJaguar, Krapht, hea3ven, cpw, SandGrainOne, Flow86, Player, asie,  AEnterprise, and all other contributors
        > Source https://github.com/BuildCraft/BuildCraft and https://github.com/BuildCraft/BuildCraftCompat
    > AgriCraft and InfinityLib
        > by RaiderOwner, codescubesandcrashes, RlonRyan, and all other contributors @ https://github.com/AgriCraft/AgriCraft and https://minecraft.curseforge.com/projects/infinitylib
    > Just Enough Items
        > by mezz and all other contributors @ https://github.com/mezz/JustEnoughItems
    > Ender IO & EnderCore
        > by CrazyPants, CyanideX, epicsquid319, Henry_Loenwind, MatthiasM, tterrag1098, and all other contributors @ https://minecraft.curseforge.com/projects/ender-io and https://minecraft.curseforge.com/projects/endercore
    > Tinkers' Construct and Mantle
        > by mDiyo, boni, jadedcat, KnightMiner, and all other contributors @ https://minecraft.curseforge.com/projects/tinkers-construct and https://minecraft.curseforge.com/projects/mantle
    > Thaumcraft and Baubles
        > by Azanor @ https://minecraft.curseforge.com/projects/thaumcraft and https://minecraft.curseforge.com/projects/baubles
    > Quark and AutoRegLib
        > by Vazkii and contributors @ https://minecraft.curseforge.com/projects/quark and https://minecraft.curseforge.com/projects/autoreglib
    > Biomes O' Plenty
        > by Glitchfiend, Adubbz, Forstride, and all its contributors @ https://minecraft.curseforge.com/projects/biomes-o-plenty
    > Dynamic Trees and Dynamic Trees - Biomes O' Plenty Compat
        > by mangoose3039 and ferreusveritas @ https://minecraft.curseforge.com/projects/dynamictrees and https://minecraft.curseforge.com/projects/dynamictrees @ https://minecraft.curseforge.com/projects/dtbop
    > Project Red (Base, Integration, Fabrication, Mechanical, Lighting, World, and Compat), MrTJPCore, Forge Relocation, and Forge Relocation - FMP Plugin
        by Mr_TJP and all of the contributors @ https://projectredwiki.com/wiki/Version_archive
    > Code ChickenLib
        > by covers1624, chicken_bones, and contributors @ https://minecraft.curseforge.com/projects/codechicken-lib-1-8
    > Forge MultiPart CBE
        > by covers1624, chicken_bones, and Mr_TJP @ https://minecraft.curseforge.com/projects/forge-multipart-cbe
### I listed all mods used in this pack and credited each to the best of my ability with respect to the authors. If you have any issues with the way I use your mod please contact me at coffeemike176@gmail.com
